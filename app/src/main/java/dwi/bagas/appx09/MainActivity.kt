package dwi.bagas.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {
    lateinit var db : SQLiteDatabase
    lateinit var lsadapter: ListAdapter
    var idLagu : Int = 0
    var idCover : Int = 0
    var laguM : String = ""

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }
    fun getDbObject() : SQLiteDatabase {
        db = DBOpenHelper(this).writableDatabase
        return db
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay -> {
                audioPlay()
            }
            R.id.btnNext -> {
                audioNext()
            }
            R.id.btnPrev -> {
                audioPrev()
            }
            R.id.btnStop -> {
                audioStop()
            }
        }
    }

    val daftarLagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)

    var posLaguSkrg = 0
    var handler= Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max = 100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnPlay.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        lsLagu.setOnItemClickListener(listClick)
        getDbObject()
    }

    override fun onStart() {
        super.onStart()
        showDataLagu()
    }
    fun showDataLagu() {
        val c: Cursor = db.rawQuery("select IDlagu as _id ,IDcover ,judul from lagu", null)
        lsadapter = SimpleCursorAdapter(
            this, R.layout.item_data_musik, c,arrayOf("_id", "IDcover", "judul"), intArrayOf(R.id.idLagu, R.id.idCover,R.id.txtLagu),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        lsLagu.adapter = lsadapter
    }
    val listClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idLagu = c.getInt(c.getColumnIndex("_id"))
        idCover = c.getInt(c.getColumnIndex("IDcover"))
        laguM = c.getString(c.getColumnIndex("judul"))
    }


    fun milliSecondToString(ms: Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        var menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(){
        if(idLagu == 0){
            Toast.makeText(this,"Pilih playlist lagu dahulu",Toast.LENGTH_SHORT).show()
        }else{
            mediaPlayer = MediaPlayer.create(this,idLagu)
            seekSong.max = mediaPlayer.duration
            txtMaxTime.setText(milliSecondToString(seekSong.max))
            txtCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
            seekSong.progress = mediaPlayer.currentPosition
            imgV.setImageResource(idCover)
            judulLagu.setText(laguM)
            mediaPlayer.start()
            var updateSeekBarThread = UpdateSeekBarProgressThread()
            handler.postDelayed(updateSeekBarThread,50)
        }
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(posLaguSkrg<(daftarLagu.size-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        audioPlay()
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying)mediaPlayer.stop()
        if(posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = daftarLagu.size - 1
        }
        audioPlay()
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    inner class UpdateSeekBarProgressThread : Runnable {
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txtCrTime.setText(milliSecondToString(currTime))
            seekSong.progress = currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this,50)
        }
    }
}
