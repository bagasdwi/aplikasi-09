package dwi.bagas.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_ver) {

    companion object{
        val DB_Name = "playlist"
        val DB_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val lagu = "create table lagu (IDlagu varchar(30) primary key , IDcover varchar(30) not null, judul text not null)"
        val insertlagu = "insert into lagu values('0x7f0b0000','0x7f060055','Iwan Fals - Aku Milikmu'),('0x7f0b0001','0x7f060056','iwan fals - belum ada judul'),('0x7f0b0002','0x7f060057','iwan fals - sore tugu Pancoran')"

        db?.execSQL(lagu)
        db?.execSQL(insertlagu)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}

